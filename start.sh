#!/bin/bash

set -eux

mkdir -p /run/chevereto/sessions

if [[ ! -d /app/data/content ]]; then
    cp -rf /app/content_orig /app/data/content
fi

if [[ ! -d /app/data/images ]]; then
    cp -rf /app/images_orig /app/data/images
fi

if [[ ! -d /app/data/app/content ]]; then
    cp -rf /app/app-content_orig /app/data/app-content
fi

if [[ ! -d /app/data/app/install ]]; then
    cp -rf /app/app-install_orig /app/data/app-install
fi

# Re-create settings.php
sed -e "s/##MYSQL_HOST##/${MYSQL_HOST}/" \
    -e "s/##MYSQL_PORT##/${MYSQL_PORT}/" \
    -e "s/##MYSQL_USERNAME##/${MYSQL_USERNAME}/" \
    -e "s/##MYSQL_PASSWORD##/${MYSQL_PASSWORD}/" \
    -e "s/##MYSQL_DATABASE##/${MYSQL_DATABASE}/" \
    /app/settings.php.template > /run/settings.php

# ensure permissions for setup
chown -R www-data.www-data /app/data /run/chevereto
# ensure permissions after setup
chown -R www-data.www-data /app/data /run

# start
echo "=> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
