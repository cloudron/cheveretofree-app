FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code
WORKDIR /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
RUN a2disconf other-vhosts-access-log && a2enmod rewrite
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/chevereto.conf /etc/apache2/sites-enabled/chevereto.conf

# configure php
RUN crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.enable 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.enable_cli 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.interned_strings_buffer 8 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.memory_consumption 128 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.save_comments 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini opcache opcache.revalidate_freq 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.save_path /run/chevereto/sessions && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.4/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/7.4/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/7.4/cli/conf.d/99-cloudron.ini

ARG VERSION=1.3.0

RUN curl -L https://github.com/Chevereto/Chevereto-Free/archive/refs/tags/${VERSION}.tar.gz | tar -xz --strip-components 1 -f -

RUN ln -s /run/settings.php /app/code/app/settings.php && \
    mv /app/code/content /app/content_orig && ln -s /app/data/content /app/code/content && \
    mv /app/code/app/content /app/app-content_orig && ln -s /app/data/app-content /app/code/app/content && \
    mv /app/code/app/install /app/app-install_orig && ln -s /app/data/app-install /app/code/app/install && \
    mv /app/code/images /app/images_orig && ln -s /app/data/images /app/code/images

ADD start.sh settings.php.template /app/

RUN chown -R www-data.www-data /app/code

CMD [ "/app/start.sh" ]
